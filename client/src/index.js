import React from "react";
import ReactDOM from "react-dom";
import 'normalize.css';
import "./index.css";
import BulkSendWallet from "./components/index";
import Provider from "./provider";

ReactDOM.render(
  <Provider>
    <BulkSendWallet />
  </Provider>,
  document.getElementById("root")
);
