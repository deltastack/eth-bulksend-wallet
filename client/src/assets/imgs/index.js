import btcaddr from './btcaddr.png'
import ethaddr from './ethaddr.png'

const imgs = {
    btcaddr,
    ethaddr
}

export default imgs;
