import React from "react";
import "./index.css";
import icons from "./icons/index";
import { withContext } from "./../../provider/index";

const Float = ({ ctx }) => {
  return (
    <div className="float">
      {icons.map((icon, index) => {
        return (
          <a key={icon.id} href={icon.href} target={icon.target}>
            <img
              onClick={() => {
                if (icon.onclick) {
                  icon.onclick(() => ctx.handleChange("openModal", true));
                }
              }}
              src={icon.src}
              title={icon.title}
              alt="icon"
            />
          </a>
        );
      })}
    </div>
  );
};

export default withContext(Float);
