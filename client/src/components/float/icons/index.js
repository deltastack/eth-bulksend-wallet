import git from "./github-logo.svg";
import medium from "./medium.svg";
import donate from "./heart.svg";
import telegram from "./telegram.svg";

const icons = [
  {
    src: donate,
    title: "Send tips",
    id: "donate-icon",
    onclick: fn => fn()
  },
  {
    src: medium,
    title: "Tutorial on medium",
    id: "medium-icon",
    target: "_blank",
    href: "https://medium.com"
  },
  {
    src: telegram,
    title: "Contact on telegram",
    id: "telegram-icon",
    target: "_blank",
    href: "https://t.me/eltneg"
  },
  {
    src: git,
    title: "View source code",
    id: "git-icon",
    target: "_blank",
    href: "https://github.com/eltneg"
  }
];

export default icons;
