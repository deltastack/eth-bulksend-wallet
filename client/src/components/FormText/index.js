import React from "react";

const FormText = props => <div className="form-text">{props.children}</div>;

export default FormText;
