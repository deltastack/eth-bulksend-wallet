import React, { Component } from "react";
import { LoadingIcon } from "./../LoadingButton/index";
import Api from "./../../utils/api/index";
import { withContext } from "./../../provider/index";

class GetForm extends Component {
  handleClick = e => {
    e.preventDefault();
    const { ctx } = this.props;
    ctx.handleChange("loading", true);
    Api.getFromGSheet({ url: ctx.url })
      .then(res => {
        if (res.error) {
          ctx.handleChange("loading", false);
          alert(`An error occured\n${res.error}`);
          return;
        }
        ctx.handleChange("loading", false);
        ctx.handleChange("data", res);
        ctx.handleChange("address", res.addresses);
        ctx.handleChange("amount", res.amounts);
        return;
      })
      .catch(e => ctx.handleChange("loading", false));
  };

  render() {
    const { ctx } = this.props;
    let Loading;
    this.props.ctx.loading
      ? (Loading = (
          <button disabled className="form-button-loading">
            Importing
            <LoadingIcon />
          </button>
        ))
      : (Loading = (
          <button className="form-button" onClick={e => this.handleClick(e)}>
            Import
          </button>
        ));
    return (
      <div className="google-form-div">
        <input
          type="url"
          onChange={e => ctx.handleChange("url", e.target.value)}
          value={ctx.url}
          placeholder="Google Sheet url"
        />
        {Loading}
      </div>
    );
  }
}

export default withContext(GetForm);
