import React from "react";
import FormText from "./../FormText/index";
import { withContext } from "./../../provider/index";
import ethApi from './../../utils/contractCall/index';

class TokenAddress extends React.Component {
  handleChange = e => {
    this.props.setSelected(e.target.value);
  };
  render() {
    const { ctx } = this.props;
    let Input;
    ctx.selected === "ethereum"
      ? (Input = (
          <input
            disabled
            value=""
            placeholder="0x00000000000000000000000000000000000000"
          />
        ))
      : (Input = (
          <input
            onChange={e => {
              ctx.handleChange("tokenAddress", e.target.value);
              if (e.target.value.length >= 42) {
                ethApi.getTokenSymbol(e.target.value).then(tokenSymbol =>
                  ctx.handleChange("tokenSymbol", tokenSymbol)
                );
              }
            }}
            value={ctx.tokenAddress}
            placeholder="Token contract address"
          />
        ));
    return (
      <div className="token-address-form-div">
        <FormText>Sending ETH or Token</FormText>
        <select
          value={ctx.selected}
          onChange={e => ctx.handleChange("selected", e.target.value)}
          className="token-select"
        >
          <option value="ethereum">Ethereum</option>
          <option value="token">Token</option>
        </select>
        {Input}
      </div>
    );
  }
}

export default withContext(TokenAddress);
