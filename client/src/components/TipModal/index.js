import React from "react";
import "./index.css";
import { withContext } from "./../../provider/index";
import imgs from "./../../assets/imgs/index";

class Modal extends React.Component {
  componentDidMount() {
    document.addEventListener("mousedown", this.handleClickOutside, false);
  }

  componentWillUnmount() {
    document.removeEventListener("mousedown", this.handleClickOutside, false);
  }

  handleClickOutside = event => {
    if (!this.node.contains(event.target)) {
      const { ctx } = this.props;
      ctx.handleChange("openModal", false);
    }
  };
  render() {
    const { ctx } = this.props;
    let openModal;
    if (ctx.openModal) {
      openModal = "modal open-modal";
    } else {
      openModal = "modal";
    }
    return (
      <div className={openModal}>
        {/* <!-- Modal content --> */}
        <div ref={node => (this.node = node)} className="modal-content">
          <span
          className="close"
            onClick={() => ctx.handleChange("openModal", false)}
          >
            &times;
          </span>
          <p>Feel free to tip me with your tokens, ethereum or bitcoin</p>
          <div className="addr-container">
            <img
              src={imgs.ethaddr}
              alt="0x0663dC599539F98F4b319fA3ceED721D63DecA7c"
            />
            <div>
              ETH: <span>0x0663dC599539F98F4b319fA3ceED721D63DecA7c</span>
            </div>
          </div>
          <div className="addr-container">
            <img src={imgs.btcaddr} alt="1MhAWz4vwSWDY5bSbyaMTqqNqjFDw6MDzx" />
            <div>
              BTC: <span>1MhAWz4vwSWDY5bSbyaMTqqNqjFDw6MDzx</span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withContext(Modal);
