import React, { Component } from "react";
import AddressAmountBox from "./AddressAmountBox/index";
import "./index.css";
import FormText from "./FormText/index";
import Header from "./Header/index";
import GetForm from "./GetForm";
import TokenAddress from "./TokenAddress/index";
import Float from './float'
import Modal from './TipModal'

export default class BulkSendWallet extends Component {
  render() {
    return (
      <div className="main">
        <Header className="header" />
        <div className="wallet-box">
          <FormText>Enter Google Sheet URL</FormText>
          <GetForm className="google-sheet-form" />
          <TokenAddress className="token-address-form" />
          <AddressAmountBox />
          <Float />
          <Modal />
        </div>
      </div>
    );
  }
}
