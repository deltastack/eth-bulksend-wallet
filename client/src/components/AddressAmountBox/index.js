import React, { Component } from "react";
import "./index.css";
import ethApi from "./../../utils/contractCall";
import { LoadingIcon } from "./../LoadingButton/index";
import FormText from "./../FormText/index";
import { withContext } from "./../../provider/index";

class AddressAmountBox extends Component {
  renderParam = () => {
    let addresses, amount;
    const { ctx } = this.props;
    addresses = [...ctx.address];
    amount = [...ctx.amount];

    return addresses.map((address, n) => (
      <tr key={`k${n}`}>
        <td key={n} data-id={n} onDoubleClick={e => this.handleDoubleClick(e)}>
          {address}
        </td>
        <td key={`m${n}`}>{amount[n]}</td>
      </tr>
    ));
  };

  setTXHash = txHash => {
    const { ctx } = this.props;
    ctx.handleChange("sending", false);
    ctx.handleChange("txHash", txHash);
  };

  handleDoubleClick = e => {
    let newAddr, newAmnt;
    const { ctx } = this.props;
    const n = e.currentTarget.dataset.id;
    if (n === -1) {
      return;
    }
    newAddr = [...ctx.address];
    newAmnt = [...ctx.amount];
    newAddr.splice(n, 1);
    newAmnt.splice(n, 1);
    ctx.handleChange("address", newAddr);
    ctx.handleChange("amount", newAmnt);
  };

  handleAddNew = e => {
    e.preventDefault();
    const { ctx } = this.props;
    if ((ctx.newAddress === "") | (ctx.newAmount === "")) {
      alert("Cannot add empty!");
      return;
    }
    ctx.handleAdd("address", ctx.newAddress);
    ctx.handleAdd("amount", ctx.newAmount);
    ctx.handleResetAddrAndAmnt();
  };

  handleSend = e => {
    e.preventDefault();
    const { ctx } = this.props;
    if (ctx.address.length === 0 || ctx.amount.length === 0) {
      alert("Cannot send to empty!");
      return;
    }

    let total = 0;
    for (const t of ctx.amount) {
      total += Number(t);
    }
    const fee = 0;
    if (ctx.selected === "ethereum") {
      const reply = window.confirm(
        `Distribute ${total} eth to ${
          ctx.address.length
        } addresses?\nMultisend Fee: ${fee} eth`
      );
      if (!reply) {
        console.log("go back");
        return;
      }
      ctx.handleChange("sending", true);

      ethApi
        .bulksend(ctx.address, ctx.amount, null, this.setTXHash)
        .then(res => {
          console.log("sent...");
        })
        .catch(err => {
          ctx.handleChange("sending", false);
          console.log("error occurred!");
        });
      
    } else {
      if(!ctx.tokenAddress || ctx.tokenAddress.length < 42){
        return window.alert("Invalid Token Address")
      }
      const reply = window.confirm(
        `Distribute ${total} ${ ctx.tokenSymbol ||'tokens'} to ${
          ctx.address.length
        } addresses?\nMultisend Fee: ${fee} eth`
      );
      if (!reply) {
        console.log("go back");
        return;
      }
      ctx.handleChange("sending", true);
      ethApi
        .bulkSendToken(
          ctx.tokenAddress,
          ctx.address,
          ctx.amount,
          null,
          this.setTXHash
        )
        .then(res => {
          console.log("sent...");
        })
        .catch(err => {
          ctx.handleChange("sending", false);
          console.log("error occurred!");
        });
    }
  };

  render() {
    const { ctx } = this.props;
    let SendButton, ShowTXHash;
    ctx.sending
      ? (SendButton = (
          <div className="box-button">
            <button className="send-button-loading">
              Sending
              <LoadingIcon />{" "}
            </button>
          </div>
        ))
      : (SendButton = (
          <div className="box-button">
            <button className="send-button" onClick={this.handleSend}>
              Send
            </button>
          </div>
        ));

    ctx.txHash
      ? (ShowTXHash = (
          <div className="tx-hash">
            <div className="tx-hash-pretext">
              Your transaction has been sent.
              <a
                href={`https://ropsten.etherscan.io/tx/${ctx.txHash}`}
                target="_blank"
              >
                Click here for details.
              </a>
              <div
                onClick={() => ctx.handleChange("txHash", "")}
                className="cancel-box"
              >
                <sup>&#x26DD;</sup>
              </div>
            </div>
          </div>
        ))
      : (ShowTXHash = <div />);
    return (
      <div className="box">
        <div className="add-new">
          <form>
            <FormText>Enter address manually</FormText>
            <input
              className="address-input"
              placeholder="Address"
              onChange={e => ctx.handleChange("newAddress", e.target.value)}
              value={ctx.newAddress}
            />
            <input
              className="amount-input"
              placeholder="Amount"
              onChange={e => ctx.handleChange("newAmount", e.target.value)}
              value={ctx.newAmount}
            />
            <button className="add-btn" onClick={e => this.handleAddNew(e)}>
              Add
            </button>
          </form>
        </div>
        <table className="address-box-table">
          <thead>
            <tr>
              <th className="address-header">Address</th>
              <th className="amount-header">Amount</th>
            </tr>
          </thead>
          <tbody>{this.renderParam()}</tbody>
        </table>
        <div className="box-button">{SendButton}</div>
        {ShowTXHash}
      </div>
    );
  }
}

export default withContext(AddressAmountBox);
