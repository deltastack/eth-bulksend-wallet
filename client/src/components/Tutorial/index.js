import React, { Component } from "react";
import googleSheetImg from "../../assets/imgs/multisend2.png";
import multisendImg from "../../assets/imgs/multisend4.png";
import "./index.css";

export default class Tutorial extends Component {
  render() {
    return (
      <div className="tutorial">
        <h2>INTRODUCING MULTISEND</h2>
        <p className="paragraph">
          Multisend is an ethereum DAPP that allows sending of ethereum and
          ethereum tokens to multiple ethereum addresses in a single
          transaction. Multisend smart contract is currently deployed on
          ethereum Kovan testnet. The DAPP can be accessed{" "}
          <a href="https://multisend.now.sh" target="_blank" rel="noopener noreferrer">
            here
          </a>
          .
        </p>
        <p className="paragraph">
          Multisend.now.sh can import addresses and the amounts to be sent to those
          addresses from google sheet.
        </p>

        <img src={googleSheetImg} alt="google sheet" />
        <p className="paragraph">
          In order to import addresses from a google sheet, four conditions must
          be satisfied:
          <ol>
            <li>
              On row 1 (red text 1), there must be two rows labeled ADDRESSES
              and AMOUNTS.
            </li>
            <li>The text of these columns must be in uppercase.</li>
            <li>Sheet name must be “Sheet1”.</li>
            <li>Each ethereum address should have a corresponding amount.</li>
          </ol>
        </p>
        <p className="paragraph">
          {" "}
          “ADDRESSES” and “AMOUNTS” may not necessarily be next to one another.
        </p>
        <img src={multisendImg} alt="multisend" />
      </div>
    );
  }
}
