import os
from flask_cors import CORS
from flask import Flask, jsonify, send_from_directory, request
from server_tools.google_sheet import SheetData

# ============================================================
app = Flask(__name__, '/build')
CORS(app)
sheet_data = SheetData("client_secret.json")

@app.route("/api/ping", methods=['GET'])
def secret():
    if request.method == 'GET':
        return jsonify({'msg': 'pong'})


@app.route("/api/get-data-from-sheet", methods=['POST'])
def param():
    req_data = request.get_json()
    if req_data:
        url = req_data.get("url")
        data = sheet_data.get_data(url)
        if isinstance(data, dict):
            return jsonify(data)
        return jsonify({'error': data})
    return jsonify({"error": "No url supplied"}), 200

@app.route("/", defaults={'path': ''})
@app.route('/<path:path>')
def index(path):
    if path != "" and os.path.exists("build/" + path):
        return send_from_directory('build', path)
    else:
        return send_from_directory('build', 'index.html')

if __name__ == "__main__":
    app.run(debug=True, use_reloader=True, port=5000, threaded=True)
