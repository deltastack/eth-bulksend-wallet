from python:3.6.5-slim-stretch

LABEL Author="eltneg"

RUN mkdir /usr/src/app

WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app

RUN pip install -r requirements.txt

COPY . /usr/src/app

EXPOSE 5000

# CMD gunicorn -w $(( 2 * `cat /proc/cpuinfo | grep 'core id' | wc -l` + 1 )) -b 0.0.0.0:5000 app:app
CMD gunicorn -w 2 -b 0.0.0.0:5000 app:app
