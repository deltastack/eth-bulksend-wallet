echo "Setting up virtual environment..."
source projenv/scripts/activate

echo "Building client..."
cd client
npm run build
cd ..

echo "Setting up server"
python3 app.py

echo "server running"
