"""Google Sheet"""
from typing import Optional, Dict, Union
import pygsheets


class SheetData(object):
    """Sheet data"""

    def __init__(self, path: str) -> None:
        """Initialise google sheet client"""
        self.client = pygsheets.authorize(service_file=path)

    def get_data(self, url: str) -> Union[str, Dict[str, list]]:
        """Get amount and address from given google sheet url"""
        try:
            work_sheet = self.client.open_by_url(url)
        except:
            return "Unable to open url"
        try:
            needed_sheet = work_sheet.worksheet_by_title("Sheet1")
        except:
            return "Sheet1 not found"
        try:
            row = needed_sheet.get_row(1)
            address_col_number = row.index("ADDRESSES") + 1
            amount_col_number = row.index("AMOUNTS") + 1
        except ValueError:
            return "NO row named 'ADDRESS or 'AMOUNT' in the first row"
        try:
            address_col: list = needed_sheet.get_col(address_col_number)
            amount_col: list = needed_sheet.get_col(amount_col_number)
        except:
            return "Unable to get required address or amount column"

        addrX = amntX = 0
        for x in address_col[::-1]:
            if x != "":
                addrX = len(address_col) - (address_col[::-1]).index(x)
                break

        for x in amount_col[::-1]:
            if x != "":
                amntX = len(amount_col) - (amount_col[::-1]).index(x)
                break

        needed_len: int = addrX if addrX >= amntX else amntX

        resp: Dict[str, list] = {
            "addresses": address_col[1:needed_len],
            "amounts": amount_col[1:needed_len]
        }
        return resp
