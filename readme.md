# Multi Send

- Send eth to multiple addresses in a single transaction
- Lower transaction fees
- Saves time

## Contribution

- Clone the repo: `$ git clone ...`
- `$ cd eth-bulksend-wallet`
- `$ docker build -t multi-send:latest .`
- `$ docker run --rm -d multi-send:latest`
- or run: `$./auto.sh` on linux and `>auto.bat` on windows

## TODO

- Send tokens
- Allow investors
- ...
